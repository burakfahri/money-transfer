package en.com.revolut.core;

import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.model.Account;
import en.com.revolut.service.AccountService;
import en.com.revolut.service.StorageService;
import en.com.revolut.utils.impl.ServiceUtils;

import java.util.Collections;
import java.util.List;

/**
 * @author burakfahri
 * Implementation of Account Service. Stores the accounts and has the crud operations.
 * @see AccountService
 */
public class AccountServiceImpl extends StorageService<Long, Account> implements AccountService {
    private static AccountService accountServiceInstance = null;
    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AccountServiceImpl.class);

    private AccountServiceImpl() {
        super();
    }

    public static synchronized AccountService getAccountServiceInstance() {
        if (accountServiceInstance == null) {
            accountServiceInstance = new AccountServiceImpl();
            logger.info("Account service instantiated");
        }

        return accountServiceInstance;
    }


    @Override
    public List<Account> getAllAccounts() {
        logger.info("returning all accounts");
        return Collections.unmodifiableList(super.getAll());
    }


    @Override
    public synchronized void addOrUpdateAccount(final Account account) throws NullParameterException {
        ServiceUtils.checkParameters(account);
        logger.info("add or update account " + account);
        super.addOrUpdateItem(account.getAccountId(), account);
    }

    @Override
    public synchronized Account removeAccount(final Long accountId) throws NullParameterException, AccountNotFoundException {
        ServiceUtils.checkParameters(accountId);
        Account account = super.getItem(accountId);
        if (account == null) {
            logger.error("account id " + accountId + " does not belongs to any account");
            throw new AccountNotFoundException();
        }
        logger.info("remove account " + account);
        return super.remove(accountId);
    }

    @Override
    public Account getAccountById(final Long accountId) throws NullParameterException {
        ServiceUtils.checkParameters(accountId);
        logger.info("account wanted for " + accountId);

        return super.getItem(accountId);
    }

    @Override
    public synchronized void removeAllAccounts() throws NullParameterException, AccountNotFoundException {
        logger.debug("removing all accounts");
        for (Account account : super.getAll()) {
            removeAccount(account.getAccountId());
        }
    }
}
