package en.com.revolut.core;

import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.AccountServiceException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.exception.TransactionException;
import en.com.revolut.model.Account;
import en.com.revolut.model.Transaction;
import en.com.revolut.service.AccountService;
import en.com.revolut.service.StorageService;
import en.com.revolut.service.TransactionService;
import en.com.revolut.utils.impl.ServiceUtils;

import java.math.BigDecimal;

/**
 * @author burakfahri
 * Implementation of Transaction Services . Provides transactions btw accounts.
 * @see TransactionService
 */
public class TransactionServiceImpl extends StorageService<Long, Transaction> implements TransactionService {
    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(TransactionServiceImpl.class);

    private static TransactionService transactionServiceInstance = null;
    private static AccountService accountServiceInstance = null;

    private TransactionServiceImpl() {
        super();
    }

    public static synchronized TransactionService getTransactionServiceInstance() {
        if (transactionServiceInstance == null) {
            transactionServiceInstance = new TransactionServiceImpl();
            logger.info("Transaction service instantiated");

        }
        return transactionServiceInstance;
    }

    public void setAccountService(final AccountService accountService) throws NullParameterException {
        ServiceUtils.checkParameters(accountService);
        logger.info("Account Service setted into Transtaction service instantiated");
        accountServiceInstance = accountService;
    }

    /**
     * Checks and returns the account by
     *
     * @param accountId
     * @return Account which has the {@param accountId}
     * @throws AccountServiceException  if the account service is not assigned
     * @throws NullParameterException   if the parameters are null
     * @throws AccountNotFoundException if the account does not exist
     */
    private Account getAccount(final Long accountId) throws AccountServiceException,
            NullParameterException, AccountNotFoundException {

        if (accountServiceInstance == null) {
            logger.error("Account service does not set");
            throw new AccountServiceException();
        }
        ServiceUtils.checkParameters(accountId);
        final Account account = accountServiceInstance.getAccountById(accountId);

        if (account == null) {
            logger.error("Account does not exit " + account);
            throw new AccountNotFoundException();
        }
        return account;
    }

    /**
     * updates the account current balance
     *
     * @param account which will update
     * @param amount  equals the current balance
     * @throws NullParameterException if the parameters are null
     */
    private void updateAccountCurrentBalance(final Account account, final BigDecimal amount) throws NullParameterException {
        ServiceUtils.checkParameters(account, amount);
        account.setCurrentBalance(amount);
        accountServiceInstance.addOrUpdateAccount(account);
    }


    @Override
    public synchronized Transaction transfer(final Long senderAccountId, final Long receiverAccountId, BigDecimal amount)
            throws AccountNotFoundException, TransactionException, NullParameterException, AccountServiceException {
        ServiceUtils.checkParameters(senderAccountId, receiverAccountId, amount);
        Account receiverAccount = getAccount(receiverAccountId);
        Account senderAccount = getAccount(senderAccountId);

        Transaction transaction = ServiceUtils.createTransferTransaction(amount, senderAccountId, receiverAccountId);

        if (amount.compareTo(senderAccount.getCurrentBalance()) > 0) {
            logger.error("ACCOUNT IS NOT ABLE TO WITHDRAW");
            throw new TransactionException("ACCOUNT IS NOT ABLE TO WITHDRAW");
        }
        updateAccountCurrentBalance(senderAccount, senderAccount.getCurrentBalance().subtract(amount));

        updateAccountCurrentBalance(receiverAccount, receiverAccount.getCurrentBalance().add(amount));

        logger.info("transfer for sender account " + senderAccount + " to " + receiverAccount + " amount =  " + amount);
        return transaction;
    }

}
