package en.com.revolut.service;

import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.model.Account;

import java.util.List;

public interface AccountService {

    /**
     * @return all the accounts in the store
     */
    List<Account> getAllAccounts();

    /**
     * adds or updates an account in the store
     *
     * @param account which will add or update on the store
     * @throws NullParameterException if the parameters are null
     */

    void addOrUpdateAccount(Account account) throws NullParameterException;


    /**
     * @param accountId belongs to Account
     * @return the removed account which has {@param accountId}
     * @throws NullParameterException   if the parameters are null
     * @throws AccountNotFoundException if the {@param accountId} does not belongs to any account
     */

    Account removeAccount(Long accountId) throws NullParameterException, AccountNotFoundException;

    /**
     * @param accountId belongs to Account
     * @return the account which has {@param accountId}
     * @throws NullParameterException if the parameters are null
     */
    Account getAccountById(Long accountId) throws NullParameterException;

    /**
     * removes all the accounts in the store
     * also removes the account and transaction links
     *
     * @throws NullParameterException   if the parameters are null
     * @throws AccountNotFoundException if the there is any accountId does not belongs to any account in the store
     */
    void removeAllAccounts() throws NullParameterException, AccountNotFoundException;
}
