package en.com.revolut.service;

import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.AccountServiceException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.exception.TransactionException;
import en.com.revolut.model.Transaction;

import java.math.BigDecimal;

public interface TransactionService {

    /**
     * Account service must use when transaction issues.
     *
     * @param accountService using for injection
     */
    void setAccountService(AccountService accountService) throws NullParameterException;


    /**
     * @param senderAccountId   sender acount id
     * @param receiverAccountId receiver acount id
     * @param amount            of the money which would be transfered.
     * @return the transaction of the transfer operation
     * @throws AccountNotFoundException if there is any account belongs to {@param senderAccountId} or {@param receiverAccountId}
     * @throws TransactionException     if the sender which has {@param senderAccountId} does not have enough money to send {@param amount
     * @throws NullParameterException   if the parameters are null
     * @throws AccountServiceException  if there is not any account belongs to {@param senderAccountId} or {@param receiverAccountId}
     * @throws IdException              if the id is not valid
     */
    Transaction transfer(Long senderAccountId, Long receiverAccountId, BigDecimal amount)
            throws AccountNotFoundException, TransactionException, NullParameterException, AccountServiceException;

}
