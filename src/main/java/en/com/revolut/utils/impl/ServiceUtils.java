package en.com.revolut.utils.impl;

import en.com.revolut.exception.NullParameterException;
import en.com.revolut.model.Transaction;

import java.math.BigDecimal;

public final class ServiceUtils {
    private ServiceUtils() {
    }

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ServiceUtils.class);

    /**
     * check the parametes are null
     *
     * @param paramters
     * @throws NullParameterException if the one of the parameter is null
     */
    public static void checkParameters(Object... paramters) throws NullParameterException {
        for (Object parameter : paramters)
            if (parameter == null) {
                logger.error("Parameter is null");
                throw new NullParameterException(new StringBuilder().append("PARAMETER IS NULL").toString());
            }
    }


    /**
     * create transaction according to its {@param transactionType}
     *
     * @param amount            of the transaction from one account to another
     * @param senderAccountId   which belongs to the account of the sender
     * @param receiverAccountId which belongs to the account of the receiver
     * @return the transaction which type is TRANSFER
     * @throws NullParameterException if the parameters are null
     */
    public static Transaction createTransferTransaction(BigDecimal amount, Long senderAccountId,
                                                        Long receiverAccountId)
            throws NullParameterException {
        checkParameters(senderAccountId, receiverAccountId, amount);
        Transaction transaction = new Transaction();

        transaction.setAmount(amount);

        transaction.setReceiverAccountId(receiverAccountId);
        transaction.setSenderAccountId(senderAccountId);
        return transaction;
    }
}
