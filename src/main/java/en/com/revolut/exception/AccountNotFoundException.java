package en.com.revolut.exception;

public class AccountNotFoundException extends Exception {
    private static final String NULL_ACCOUNT = "ACCOUNT DOES NOT EXIST";

    public AccountNotFoundException() {
        super(NULL_ACCOUNT);
    }

    public AccountNotFoundException(String message) {
        super(message);
    }
}
