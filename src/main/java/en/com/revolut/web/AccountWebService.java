package en.com.revolut.web;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import en.com.revolut.core.AccountServiceImpl;
import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.model.Account;
import en.com.revolut.service.AccountService;
import en.com.revolut.utils.web.WebUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

@Path("/accounts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountWebService {
    private final AccountService accountService = AccountServiceImpl.getAccountServiceInstance();
    private static final String JSON_IS_NOT_VALID = "Json is not valid";
    private static final String ID_IS_NOT_VALID = "Id is not valid";
    private final Logger log = Logger.getLogger(AccountWebService.class);
    private Gson gson = new Gson();


    @GET
    public Response getAllAccounts() {
        String accountListJson = gson.toJson(accountService.getAllAccounts());
        log.debug(accountListJson);
        return Response.ok(accountListJson).build();
    }

    @GET
    @Path("/{accountId}")
    public Response getAccountByAccountId(@PathParam("accountId") Long accountId) {
        Account account;
        try {
            account = accountService.getAccountById(accountId);
            if (account == null)
                return Response.status(Response.Status.NOT_FOUND).entity("Account does not exist").build();
        } catch (NullParameterException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Account id is not acceptable").build();
        }
        return Response.ok(gson.toJson(account)).build();
    }


    @POST
    public Response createAccount(String stringAccount, @Context UriInfo uriInfo) {

        URI uri;
        try {

            log.info(uriInfo);
            Account account = gson.fromJson(stringAccount, Account.class);
            if (account == null)
                return Response.status(Response.Status.BAD_REQUEST).entity("Accound must not" +
                        "be null while creating new account").build();
            if (accountService.getAccountById(account.getAccountId()) != null)
                return Response.status(Response.Status.BAD_GATEWAY).entity("Account already exists").build();

            uri = WebUtils.generateUri(uriInfo, account.getAccountId());
            accountService.addOrUpdateAccount(account);
        } catch (NullParameterException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Account model is wrong").build();
        } catch (JsonSyntaxException je) {
            return Response.status(Response.Status.BAD_REQUEST).entity(JSON_IS_NOT_VALID).build();
        }

        return Response.created(uri).build();
    }

    @PUT
    @Path("/{accountId}")
    public Response updateAccount(String stringAccount, @PathParam("accountId") Long accountId) {

        Account account;

        try {
            account = gson.fromJson(stringAccount, Account.class);
            Account oldAccount = accountService.getAccountById(accountId);
            if (oldAccount == null)
                return Response.status(Response.Status.NOT_FOUND).entity(accountId).build();

            account.setAccountId(accountId);
            accountService.addOrUpdateAccount(account);

        } catch (NullParameterException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Account model is wrong").build();
        } catch (JsonSyntaxException je) {
            return Response.status(Response.Status.BAD_REQUEST).entity(JSON_IS_NOT_VALID).build();
        }
        return Response.ok().entity(gson.toJson(account)).build();
    }

    @DELETE
    @Path("/{accountId}")
    public Response deleteAccount(@PathParam("accountId") Long accountId) {
        Account account;
        try {
            account = accountService.removeAccount(accountId);
            if (account == null)
                return Response.status(Response.Status.NOT_FOUND).entity("Account is not found " + accountId).build();
        } catch (NullParameterException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Account id is null").build();
        } catch (AccountNotFoundException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ID_IS_NOT_VALID).build();
        }
        return Response.ok(gson.toJson(account)).build();
    }


}
