package en.com.revolut.web;

import com.google.gson.Gson;
import en.com.revolut.core.TransactionServiceImpl;
import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.AccountServiceException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.exception.TransactionException;
import en.com.revolut.model.Transaction;
import en.com.revolut.service.TransactionService;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@Path("/transactions")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TransactionWebService {

    private final TransactionService transactionService = TransactionServiceImpl.getTransactionServiceInstance();

    private static final Logger log = Logger.getLogger(AccountWebService.class);
    private Gson gson = new Gson();


    @POST
    @Path("/from/{senderAccountId}/to/{receiverAccountId}/amount/{amount}")
    public Response transfer(@PathParam("senderAccountId") Long senderAccountId, @PathParam("receiverAccountId")
            Long receiverAccountId, @PathParam("amount") BigDecimal amount) {
        String transactionJson;
        try {
            Transaction transaction = transactionService.transfer(senderAccountId, receiverAccountId, amount);
            transactionJson = gson.toJson(transaction);
        } catch (NullParameterException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Account ids or amount can not be null")
                    .build();
        } catch (AccountNotFoundException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Account ids or amount are not be valid").build();
        } catch (AccountServiceException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Account Service is not valid").build();
        } catch (TransactionException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Sender account has not got enough money").build();
        }
        log.debug(transactionJson);
        return Response.ok(transactionJson).build();
    }


}
