package en.com.revolut.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * an instance of exchanging money
 */
public class Transaction {
    Long senderAccountId;//account id of sender side , it is null when withdraw transactions
    Long receiverAccountId;////account id of receiver side , it is null when deposit transactions
    BigDecimal amount;//amont of the transaction
}
