package en.com.revolut.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * Bank account of the Customer
 */
public class Account {
    private Long accountId; //Id of the account ,starts with ACC-{id}
    private BigDecimal currentBalance; //current balance of the account
}
