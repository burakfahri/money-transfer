package en.com.revolut;

import en.com.revolut.core.AccountServiceImpl;
import en.com.revolut.core.TransactionServiceImpl;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.service.AccountService;
import en.com.revolut.service.TransactionService;
import en.com.revolut.web.AccountWebService;
import en.com.revolut.web.TransactionWebService;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;


public class Main {

    private static AccountService accountService = null;
    private static TransactionService transactionService = null;
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Main.class);


    public static void main(String[] args) {
        try {

            init();
            log.info("PROGRAM HAS BEEN STARTED");
            startService();
            log.info("SERVICE HAS BEEN STARTED");
        } catch (NullParameterException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * initiates the services
     */
    private static void init() throws NullParameterException {
        accountService = AccountServiceImpl.getAccountServiceInstance();
        transactionService = TransactionServiceImpl.getTransactionServiceInstance();
        transactionService.setAccountService(accountService);


    }

    /**
     * start the embeded jetty service
     */
    private static void startService() {
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                AccountWebService.class.getCanonicalName() + "," + TransactionWebService.class.getCanonicalName());
        try {
            server.start();
            server.join();
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            server.destroy();
        }
    }
}
