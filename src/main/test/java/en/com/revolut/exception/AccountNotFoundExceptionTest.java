/**
 *
 */
package en.com.revolut.exception;

import org.junit.Assert;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Assertions;
import pl.pojo.tester.api.assertion.Method;


public class AccountNotFoundExceptionTest {


    @Test(expected = AccountNotFoundException.class)
    public void testConstructor() throws AccountNotFoundException {
        AccountNotFoundException exception = new AccountNotFoundException("Dummy");
        Assert.assertNotNull("AccountNotFoundException can not be null", exception);
        throw exception;
    }

    @Test
    public void testConstructorAuto() {
        Assertions.assertPojoMethodsFor(AccountNotFoundException.class).testing(Method.CONSTRUCTOR).areWellImplemented();
    }
}
