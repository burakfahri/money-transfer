package en.com.revolut.core;

import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.model.Account;
import en.com.revolut.service.AccountService;
import en.com.revolut.service.TransactionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountServiceImplTest {
    public static List<Account> accountList = new ArrayList<>();
    private AccountService accountService = null;
    private TransactionService transactionService = null;

    public static List<Account> createMockAccount(int count) {
        for (long i = 0; i < count; i++) {
            Account account = new Account();
            account.setAccountId(i);
            account.setCurrentBalance(new BigDecimal(100.0));
            accountList.add(account);
        }

        return accountList;

    }

    @Before
    public void setup() throws NullParameterException, AccountNotFoundException {
        accountService = AccountServiceImpl.getAccountServiceInstance();
        transactionService = TransactionServiceImpl.getTransactionServiceInstance();
        transactionService.setAccountService(accountService);
        AccountServiceImplTest.accountList.clear();
        accountService.removeAllAccounts();
    }

    @Test
    public void addOrUpdateAccountTest() throws NullParameterException {

        //ADD ACCOUNT
        List<Account> accounts = createMockAccount(1);
        accountService.addOrUpdateAccount(accounts.get(0));
        List<Account> accountList = accountService.getAllAccounts();
        Assertions.assertEquals(accountList, accounts);


        //UPDATE ACCOUNT
        Account account = accounts.get(0);
        accountService.addOrUpdateAccount(account);
        accountList = accountService.getAllAccounts();
        Assertions.assertEquals(accountList, accounts);

    }


    @Test
    public void removeAccount() throws NullParameterException, AccountNotFoundException {


        List<Account> accounts = createMockAccount(1);
        accountService.addOrUpdateAccount(accounts.get(0));
        Assertions.assertEquals(accountService.getAllAccounts().size(), 1);
        accountService.removeAccount(accounts.get(0).getAccountId());
        Assertions.assertEquals(accountService.getAllAccounts().size(), 0);
        List<Account> accountList1 = accountService.getAllAccounts();
        Assertions.assertNotEquals(accountList1, accounts);

    }

    @Test(expected = AccountNotFoundException.class)
    public void removeAccountWithExceptionTest() throws NullParameterException, AccountNotFoundException {
        List<Account> accounts = createMockAccount(1);
        accountService.removeAccount(accounts.get(0).getAccountId());

    }

    @Test
    public void getAccountByIdTest() throws NullParameterException {


        List<Account> accounts = createMockAccount(1);
        accountService.addOrUpdateAccount(accounts.get(0));
        Assertions.assertEquals(accountService.getAccountById(accounts.get(0).getAccountId()), accounts.get(0));

    }

}
