package en.com.revolut.core;

import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.AccountServiceException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.exception.TransactionException;
import en.com.revolut.model.Transaction;
import en.com.revolut.service.AccountService;
import en.com.revolut.service.TransactionService;
import en.com.revolut.utils.impl.ServiceUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TransactionServiceImplTest {
    public static List<Transaction> transactionList = new ArrayList<>();
    private AccountService accountService = null;
    private TransactionService transactionService = null;


    public static List<Transaction> createMockTransaction(int count) throws NullParameterException {
        ServiceUtils.checkParameters(count);
        AccountServiceImplTest.createMockAccount(2);

        for (int i = 0; i < count; i++) {
            Transaction transaction = new Transaction();
            transaction.setSenderAccountId(AccountServiceImplTest.accountList.get(0).getAccountId());
            transaction.setReceiverAccountId(AccountServiceImplTest.accountList.get(1).getAccountId());
            transaction.setAmount(new BigDecimal(100.0));
            transactionList.add(transaction);
        }

        return transactionList;
    }

    @Before
    public void setup() throws NullParameterException {
        accountService = AccountServiceImpl.getAccountServiceInstance();
        transactionService = TransactionServiceImpl.getTransactionServiceInstance();
        transactionService.setAccountService(accountService);
    }


    @Test
    public void transferTest() throws NullParameterException, AccountNotFoundException, AccountServiceException, TransactionException {
        AccountServiceImplTest.createMockAccount(2);
        accountService.addOrUpdateAccount(AccountServiceImplTest.accountList.get(0));
        accountService.addOrUpdateAccount(AccountServiceImplTest.accountList.get(1));
        transactionService.transfer(AccountServiceImplTest.accountList.get(0).getAccountId()
                , AccountServiceImplTest.accountList.get(1).getAccountId(), new BigDecimal(100.0));
        Assertions.assertEquals(accountService.getAccountById(AccountServiceImplTest.accountList.get(1).getAccountId()).getCurrentBalance(), new BigDecimal(200));
        Assertions.assertEquals(accountService.getAccountById(AccountServiceImplTest.accountList.get(0).getAccountId()).getCurrentBalance(), new BigDecimal(0));
        accountService.removeAccount(AccountServiceImplTest.accountList.get(0).getAccountId());
        accountService.removeAccount(AccountServiceImplTest.accountList.get(1).getAccountId());
        AccountServiceImplTest.accountList = new ArrayList<>();
    }


}
