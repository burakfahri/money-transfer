package en.com.revolut.web;

import com.google.gson.Gson;
import en.com.revolut.core.AccountServiceImpl;
import en.com.revolut.core.TransactionServiceImpl;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.model.Account;
import en.com.revolut.service.AccountService;
import en.com.revolut.service.TransactionService;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class WebServiceTest {

    private static Server server = null;
    private static HttpClient client;
    private static PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
    private URIBuilder builder = new URIBuilder().setScheme("http").setHost("localhost:8084");
    protected Gson gson = new Gson();
    protected static AccountService accountService = null;
    protected static TransactionService transactionService = null;
    protected static TransactionWebService transactionWebService = null;
    protected static AccountWebService accountWebService = null;

    @BeforeClass
    public static void setup() throws Exception {
        startServer();
        connManager.setDefaultMaxPerRoute(100);
        connManager.setMaxTotal(200);

        client = HttpClients.custom()
                .setConnectionManager(connManager)
                .setConnectionManagerShared(true)
                .build();
        init();
    }

    @AfterClass
    public static void closeClient() throws Exception {
        //server.stop();
        HttpClientUtils.closeQuietly(client);
    }


    public List<Account> createMockAccountList(int count) {
        List<Account> accountList = new ArrayList<>();

        for (long i = 0; i < count; i++) {
            Account account = new Account();
            account.setCurrentBalance(new BigDecimal(100.0));
            account.setAccountId(i);
            accountList.add(account);
        }
        return accountList;
    }

    public List<Account> createMockAccountListForTransaction(int count) throws NullParameterException {
        List<Account> accountList = createMockAccountList(count);
        for (Account account : accountList) {
            accountService.addOrUpdateAccount(account);
        }
        return accountList;
    }

    private HttpPost createHttpPost(URI uri, String entityString) throws UnsupportedEncodingException {
        HttpPost request = new HttpPost();
        request.setURI(uri);
        fillHttpRequest(request, entityString);
        return request;

    }

    private HttpPut createHttpPut(URI uri, String entityString) throws UnsupportedEncodingException {
        HttpPut request = new HttpPut();
        request.setURI(uri);
        fillHttpRequest(request, entityString);
        return request;
    }

    public HttpResponse executeHttpRequestBase(String uriStr, String methodName) throws IOException, URISyntaxException {
        HttpRequestBase httpRequestBase = null;
        if (methodName.equals(HttpDelete.METHOD_NAME))
            httpRequestBase = new HttpDelete();
        else if (methodName.equals(HttpGet.METHOD_NAME))
            httpRequestBase = new HttpGet();
        if (httpRequestBase == null)
            return null;
        URI uri = builder.setPath(uriStr).build();
        httpRequestBase.setURI(uri);
        httpRequestBase.setHeader("Content-type", "application/json");
        return client.execute(httpRequestBase);
    }


    public HttpResponse executeHttpEntityEnclosingRequestBase(String uriStr, String entityString, String methodName) throws IOException, URISyntaxException {
        HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase = null;
        URI uri = builder.setPath(uriStr).build();
        if (methodName.equals(HttpPost.METHOD_NAME))
            httpEntityEnclosingRequestBase = createHttpPost(uri, entityString);
        else if (methodName.equals(HttpPut.METHOD_NAME))
            httpEntityEnclosingRequestBase = createHttpPut(uri, entityString);
        if (httpEntityEnclosingRequestBase == null)
            return null;
        return client.execute(httpEntityEnclosingRequestBase);
    }


    private void fillHttpRequest(HttpEntityEnclosingRequestBase request, String entityString) throws UnsupportedEncodingException {
        if (entityString != null) {
            StringEntity entity = new StringEntity(entityString);
            request.setEntity(entity);
        }
        request.setHeader("Content-type", "application/json");
    }

    private static void init() throws NullParameterException {
        accountService = AccountServiceImpl.getAccountServiceInstance();
        transactionService = TransactionServiceImpl.getTransactionServiceInstance();
        transactionService.setAccountService(accountService);

    }

    private static void startServer() throws Exception {
        if (server == null) {
            server = new Server(8084);
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setContextPath("/");
            server.setHandler(context);
            ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
            servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                    AccountWebService.class.getCanonicalName() + "," + TransactionWebService.class.getCanonicalName());
            server.start();
        }
    }
}
