package en.com.revolut.web;

import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.exception.TransactionException;
import en.com.revolut.model.Account;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Burak Cabuk .
 */
public class AccountWebServiceITTest extends WebServiceTest {
    @Before
    public void before() throws NullParameterException, AccountNotFoundException, TransactionException {
        accountService.removeAllAccounts();
    }

    @Test
    public void testGetAllAccounts() throws NullParameterException, URISyntaxException, IOException {
        List<Account> accountList = createMockAccountList(100);

        for (Account account : accountList) {
            accountService.addOrUpdateAccount(account);
        }
        HttpResponse response = executeHttpRequestBase("/accounts", HttpGet.METHOD_NAME);
        int statusCode = response.getStatusLine().getStatusCode();

        assertTrue(statusCode == Response.Status.OK.getStatusCode());
        String jsonString = EntityUtils.toString(response.getEntity());
        List<Account> accounts = gson.fromJson(jsonString, ArrayList.class);
        assertEquals(accounts.size(), accountList.size());
    }

    @Test
    public void accountNotFoundException() throws IOException, URISyntaxException {
        //getbyid
        HttpResponse response = executeHttpRequestBase("/accounts/1", HttpGet.METHOD_NAME);
        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.NOT_FOUND.getStatusCode());


        //delete
        HttpResponse httpResponse = executeHttpRequestBase("/accounts/" + 1
                , HttpDelete.METHOD_NAME);
        statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.NOT_FOUND.getStatusCode());

        //delete
        List<Account> accountList = createMockAccountList(1);
        httpResponse = executeHttpEntityEnclosingRequestBase("/accounts/" +
                accountList.get(0).getAccountId(), gson.toJson(accountList.get(0)), HttpPut.METHOD_NAME);
        statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    public void testGetAccountByAccountId() throws NullParameterException, URISyntaxException, IOException {
        List<Account> accountList = createMockAccountList(1);
        for (Account account : accountList) {
            accountService.addOrUpdateAccount(account);
        }

        //Success
        HttpResponse response = executeHttpRequestBase("/accounts/" + accountList.get(0).getAccountId()
                , HttpGet.METHOD_NAME);

        int statusCode = response.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.OK.getStatusCode());
    }

    @Test
    public void testAccountAlreadyExist() throws URISyntaxException, IOException {
        List<Account> accountList = createMockAccountList(1);
        //Success
        HttpResponse httpResponse = executeHttpEntityEnclosingRequestBase("/accounts"
                , gson.toJson(accountList.get(0)), HttpPost.METHOD_NAME);
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.CREATED.getStatusCode());

        httpResponse = executeHttpEntityEnclosingRequestBase("/accounts"
                , gson.toJson(accountList.get(0)), HttpPost.METHOD_NAME);
        statusCode = httpResponse.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.BAD_GATEWAY.getStatusCode());

    }

    @Test
    public void testCreateAccount() throws URISyntaxException, IOException {
        List<Account> accountList = createMockAccountList(1);
        //Success
        HttpResponse httpResponse = executeHttpEntityEnclosingRequestBase("/accounts"
                , gson.toJson(accountList.get(0)), HttpPost.METHOD_NAME);
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.CREATED.getStatusCode());
        //Corrupt Json
        httpResponse = executeHttpEntityEnclosingRequestBase("/accounts"
                , "{fail}", HttpPost.METHOD_NAME);
        statusCode = httpResponse.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    public void testJsonSyntaxException() throws URISyntaxException, IOException {
        //create
        Integer i = 0;
        HttpResponse httpResponse = executeHttpEntityEnclosingRequestBase("/accounts"
                , gson.toJson(i), HttpPost.METHOD_NAME);
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.BAD_REQUEST.getStatusCode());

        //update
        httpResponse = executeHttpEntityEnclosingRequestBase("/accounts/" +
                i, gson.toJson(i), HttpPut.METHOD_NAME);
        statusCode = httpResponse.getStatusLine().getStatusCode();
        assertTrue(statusCode == Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    public void testUpdateAccount() throws NullParameterException, URISyntaxException, IOException {
        List<Account> accountList = createMockAccountList(1);
        for (Account account : accountList) {
            accountService.addOrUpdateAccount(account);

            HttpResponse httpResponse = executeHttpEntityEnclosingRequestBase("/accounts/" +
                    account.getAccountId(), gson.toJson(account), HttpPut.METHOD_NAME);

            int statusCode = httpResponse.getStatusLine().getStatusCode();
            assertTrue(statusCode == Response.Status.OK.getStatusCode());
        }
    }

    @Test
    public void testDeleteAccount() throws NullParameterException, URISyntaxException, IOException {
        List<Account> accountList = createMockAccountList(1);
        accountService.addOrUpdateAccount(accountList.get(0));

        HttpResponse httpResponse = executeHttpRequestBase("/accounts/" + accountList.get(0).getAccountId()
                , HttpDelete.METHOD_NAME);
        assertTrue(httpResponse.getStatusLine().getStatusCode() == Response.Status.OK.getStatusCode());

    }


}
