package en.com.revolut.web;

import en.com.revolut.exception.AccountNotFoundException;
import en.com.revolut.exception.NullParameterException;
import en.com.revolut.exception.TransactionException;
import en.com.revolut.model.Account;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TransactionWebServiceITTest extends WebServiceTest {

    @Before
    public void before() throws NullParameterException, AccountNotFoundException, TransactionException {
        accountService.removeAllAccounts();
    }

    @Test
    public void testTransfer() throws URISyntaxException, IOException, NullParameterException {
        List<Account> mockAccountList = createMockAccountListForTransaction(2);
        Account mockSenderAccount = mockAccountList.get(0);
        Account mockReceiverAccount = mockAccountList.get(1);
        HttpResponse httpResponse = executeHttpEntityEnclosingRequestBase("/transactions/from/" +
                mockSenderAccount.getAccountId() + "/to/" +
                mockReceiverAccount.getAccountId() +
                "/amount/100", null, HttpPost.METHOD_NAME);
        assertEquals(200, httpResponse.getStatusLine().getStatusCode());
    }


    @Test
    public void testAccountNotFound() throws URISyntaxException, IOException {
        HttpResponse httpResponse = executeHttpEntityEnclosingRequestBase("/transactions/from/" +
                1 + "/to/" +
                2 +
                "/amount/100", null, HttpPost.METHOD_NAME);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), httpResponse.getStatusLine().getStatusCode());
    }

    @Test
    public void testTransferException() throws URISyntaxException, IOException, NullParameterException {
        List<Account> mockAccountList = createMockAccountListForTransaction(2);
        Account mockSenderAccount = mockAccountList.get(0);
        Account mockReceiverAccount = mockAccountList.get(1);
        HttpResponse httpResponse = executeHttpEntityEnclosingRequestBase("/transactions/from/" +
                mockSenderAccount.getAccountId() + "/to/" +
                mockReceiverAccount.getAccountId() +
                "/amount/300", null, HttpPost.METHOD_NAME);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), httpResponse.getStatusLine().getStatusCode());
    }
}
