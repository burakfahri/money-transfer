package en.com.revolut.model;

import org.junit.Assert;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Assertions;
import pl.pojo.tester.api.assertion.Method;

import java.math.BigDecimal;

public class AccountTest {
    @Test
    public void testAccountGetterAndToString() {
        Assertions.assertPojoMethodsFor(Account.class).testing(Method.GETTER, Method.TO_STRING).areWellImplemented();
    }

    @Test
    public void testAccountConstructorAndSetters() {
        Account account = new Account(1l, new BigDecimal(100.0));
        Account account1 = new Account(1l, new BigDecimal(100.0));
        Account account2 = new Account(2l, new BigDecimal(200.0));
        Account account3 = new Account();
        account3.setAccountId(1l);
        account3.setCurrentBalance(new BigDecimal(100.0));
        Assert.assertTrue(account.equals(account1));
        Assert.assertEquals(account, account3);
        Assert.assertNotEquals(account, account2);
    }


}
