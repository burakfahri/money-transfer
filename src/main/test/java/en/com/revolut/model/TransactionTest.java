package en.com.revolut.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import pl.pojo.tester.api.assertion.Assertions;
import pl.pojo.tester.api.assertion.Method;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionTest {
    @Test
    public void testTransaction() {
        Assertions.assertPojoMethodsFor(Transaction.class).testing(Method.GETTER, Method.TO_STRING).areWellImplemented();
    }

    @org.junit.Test
    public void testAccountConstructorAndSetters() {
        Date date = new Date();

        Transaction transaction = new Transaction(1l, 2l, new BigDecimal(100.0));

        Transaction transaction1 = new Transaction(1l, 2l, new BigDecimal(100.0));

        Transaction transaction2 = new Transaction();
        Transaction transaction3 = new Transaction();

        transaction3.setSenderAccountId(1l);
        transaction3.setReceiverAccountId(2l);
        transaction3.setAmount(new BigDecimal(100.0));
        Assert.assertTrue(transaction.equals(transaction1));
        Assert.assertEquals(transaction, transaction3);
        Assert.assertNotEquals(transaction, transaction2);
    }
}
