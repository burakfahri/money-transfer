# money-transfer 

---
Money transfer project allows people the transfer money between accounts

## build

```
mvn clean install compile assembly:single
```
## run

```
java -jar ~/.m2/repository/pl/com/revolut/money-transfer-main/1.0/money-transfer-main-1.0-jar-with-dependencies.jar
```
## logs file
```
./logs/money-transfer
```

### Available Account Services

| HTTP METHOD | PATH | USAGE |
| -----------| ------ | ------ |
| GET | /accounts/ | get all accounts | 
| GET | /accounts/{accountId} | get account by id | 
| POST | /accounts | create a new account | 
| PUT | /accounts/{accountId} | update an account | 
| DELETE | /accounts/{accountId} | remove an account | 

### Available Account Services

| HTTP METHOD | PATH | USAGE |
| -----------| ------ | ------ |
| POST | /transactions/from/{senderAccountId}/to/{receiverAccountId}/amount/{amount} | transfer money from one acount to another | 

### Http Status
- 200 OK: The request has succeeded
- 201 Created: The request has created
- 400 Bad Request: The request could not be understood by the server 
- 404 Not Found: The requested resource cannot be found
- 500 Internal Server Error: The server encountered an unexpected condition 

## Sample Jsons
___

### account
```
{
  "accountId": 1,
  "currentBalance": 100,
}
```
### transaction
```
{
  "senderCustomerId": 1,
  "receiverCustomerId": 2,
  "amount": 100,
}
```

### example posts via curl

```

POST ACCOUNTS
curl -H "Content-Type:application/json" -X POST -d @accountEvent.json http://localhost:8080/accounts

GET ACCOUNTS
curl -H "Content-Type:application/json" -X GET http://localhost:8080/accounts | python -m json.tool

QUERY ACCOUNTS
curl -H "Content-Type:application/json" -X GET http://localhost:8080/accounts/{1}

TRANSFER ACCOUNTS
curl -H "Content-Type:application/json" -X POST http://localhost:8080/transactions/transfer/from/{1}/to/2/amount/{100}

```